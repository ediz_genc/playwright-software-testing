# About

This project is a repository for E2E testing using Playwright.

# Playwright

This part will introduce some Playwright operations. Official documents are [here](https://playwright.dev/docs/intro).


# SUT

A Web [Visual Novel Editor](https://github.com/Kirilllive/tuesday-js),please check the [tutorial](https://kirilllive.github.io/tuesday-js/doc_editor.html#quick_tutorial)

## Run Tests
```
npx playwright test

// run tests in UI mode
npx playwright test --ui
```

## Debug Tests
t.ex. in browser Chrome.

```
npx playwright test --project=chromium --debug
```

## Show Reports
```
npx playwright show-report
```

## Write Tests

[Manually](https://playwright.dev/docs/writing-tests) and/or [Generating Code from Actions](https://playwright.dev/docs/codegen-intro) like click or fill by 
simply interacting with the page.

```
npx playwright codegen <URL>
```